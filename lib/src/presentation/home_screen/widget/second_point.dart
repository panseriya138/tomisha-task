import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:tomishatask/src/core/constant/color_constant.dart';
import 'package:tomishatask/src/shared_widget/responsive/responsive_extensions.dart';

import '../../../core/theme/app_text_theme.dart';

class SecondPoint extends StatelessWidget {
  const SecondPoint({
    Key? key,
    required this.title,
    required this.imagePath,
    required this.index,
  }) : super(key: key);

  final String title;
  final String imagePath;
  final int index;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.sizeOf(context).width;

    return Column(
      children: [
        Transform.translate(
          offset: const Offset(0, -60),
          child: ClipPath(
            clipper: ProsteBezierCurve(
              position: ClipPosition.top,
              list: [
                BezierCurveSection(
                  start: Offset(screenWidth, 10),
                  top: Offset(screenWidth / 4 * 3, 0),
                  end: Offset(screenWidth / 2, 30),
                ),
                BezierCurveSection(
                  start: Offset(screenWidth / 2, 0),
                  top: Offset(screenWidth / 4, 30),
                  end: const Offset(0, 0),
                ),
              ],
            ),
            child: ClipPath(
              clipper: ProsteBezierCurve(
                position: ClipPosition.bottom,
                list: [
                  BezierCurveSection(
                    start: const Offset(0, 450),
                    top: Offset(screenWidth / 4, 420),
                    end: Offset(screenWidth / 2, 425),
                  ),
                  BezierCurveSection(
                    start: Offset(screenWidth / 2, 425),
                    top: Offset(screenWidth / 4 * 3, 450),
                    end: Offset(screenWidth, 425),
                  ),
                ],
              ),
              child: Container(
                height: 470,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      ColorConstant.green_200,
                      ColorConstant.background,
                    ],
                  ),
                ),
                child: Stack(
                  children: [
                    if (index == 1)
                      Positioned(
                        bottom: 0,
                        top: 70,
                        left: 60,
                        child: Center(
                          child: SvgPicture.asset(
                            imagePath,
                            height: 230,
                            width: index == 1 ? 259 : 220,
                          ),
                        ),
                      ),
                    Column(
                      children: [
                        const SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 37),
                          child: Stack(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    '2.',
                                    style: AppTextTheme(context).h1?.copyWith(
                                          fontSize: 156,
                                          color: ColorConstant.fontColor,
                                        ),
                                  ),
                                  SizedBox(width: 23.w),
                                  Flexible(
                                    child: Transform.translate(
                                      offset: const Offset(0, -35),
                                      child: Text(
                                        title,
                                        style: AppTextTheme(context)
                                            .body
                                            ?.copyWith(
                                              color: ColorConstant.fontColor,
                                            ),
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        if (index != 1) ...[
                          const SizedBox(
                            height: 25,
                          ),
                          SvgPicture.asset(
                            imagePath,
                            height: index == 0
                                ? 127
                                : index == 1
                                    ? 179
                                    : 148,
                            width: index == 0
                                ? 187
                                : index == 1
                                    ? 259
                                    : 220,
                          ),
                        ]
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
