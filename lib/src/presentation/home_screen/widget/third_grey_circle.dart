import 'package:flutter/material.dart';
import 'package:tomishatask/src/core/constant/color_constant.dart';
import 'package:tomishatask/src/shared_widget/responsive/responsive_extensions.dart';

class ThirdGreyCircle extends StatelessWidget {
  const ThirdGreyCircle({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: -40,
      bottom: 250,
      child: Container(
        height: 303.h,
        width: 303.w,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: ColorConstant.greyCircle,
        ),
      ),
    );
  }
}
