import 'package:flutter/material.dart';
import 'package:tomishatask/src/core/constant/assets_constant.dart';
import 'package:tomishatask/src/core/constant/string_constants.dart';
import 'package:tomishatask/src/presentation/home_screen/widget/first_point.dart';
import 'package:tomishatask/src/presentation/home_screen/widget/second_point.dart';
import 'package:tomishatask/src/presentation/home_screen/widget/third_grey_circle.dart';
import 'package:tomishatask/src/presentation/home_screen/widget/third_point.dart';
import 'package:tomishatask/src/shared_widget/adaptive/adaptive_base_widget.dart';

class ArbeitnehmerView extends StatelessWidget {
  const ArbeitnehmerView({Key? key, required this.selectedIndex})
      : super(key: key);

  final int selectedIndex;

  @override
  Widget build(BuildContext context) {
    return AdaptiveBuilder(
      builder: (context, sizingInformation) => Stack(
        children: [
          if (sizingInformation.isWebMobile || sizingInformation.isNativeMobile)
            const ThirdGreyCircle(),
          Column(
            children: [
              const FirstPoint(
                imagePath: AssetsConstant.undrawProfileData,
                title: StringConstant.secondPointWeb,
              ),
              SecondPoint(
                title: StringConstant.secondPointWeb,
                imagePath: AssetsConstant.undrawTask,
                index: selectedIndex,
              ),
              ThirdPoint(
                imagePath: AssetsConstant.undrawPersonalFile,
                title: StringConstant.arbitmerThirdPoint,
                index: selectedIndex,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
