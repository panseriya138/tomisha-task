import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomishatask/src/core/constant/color_constant.dart';
import 'package:tomishatask/src/shared_widget/responsive/responsive_extensions.dart';

import '../../../core/theme/app_text_theme.dart';

class ThirdPoint extends StatelessWidget {
  const ThirdPoint({
    Key? key,
    required this.title,
    required this.imagePath,
    required this.index,
  }) : super(key: key);

  final String title;
  final String imagePath;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Transform.translate(
          offset: const Offset(0, -60),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 56),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      '3.',
                      style: AppTextTheme(context).h1?.copyWith(
                            fontSize: 156,
                            color: ColorConstant.fontColor,
                          ),
                    ),
                    SizedBox(width: 23.w),
                    Flexible(
                      child: Transform.translate(
                        offset: const Offset(0, -60),
                        child: Wrap(
                          children: [
                            Text(
                              title,
                              style: AppTextTheme(context).body?.copyWith(
                                    color: ColorConstant.fontColor,
                                  ),
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SvgPicture.asset(
                imagePath,
                height: index == 0
                    ? 210
                    : index == 1
                        ? 197
                        : 190,
                width: index == 0
                    ? 281
                    : index == 1
                        ? 240
                        : 250,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
