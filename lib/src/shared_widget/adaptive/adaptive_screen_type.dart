enum AdaptiveScreenType {
  nativeMobile,
  nativeTablet,
  webMobile,
  webTablet,
  desktop,
  desktopLarge
}
