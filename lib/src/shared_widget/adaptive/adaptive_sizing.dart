import 'package:flutter/material.dart';
import 'package:tomishatask/src/shared_widget/adaptive/adaptive_screen_type.dart';

class AdaptiveSizingInfo {
  final AdaptiveScreenType deviceType;
  final Size screenSize;
  final Size localWidgetSize;

  AdaptiveSizingInfo({
    required this.deviceType,
    required this.screenSize,
    required this.localWidgetSize,
  });

  @override
  String toString() {
    return 'DeviceType:$deviceType ScreenSize:$screenSize LocalWidgetSize:$localWidgetSize';
  }

  bool get isDesktopLarge =>
      deviceType == AdaptiveScreenType.desktop && screenSize.width > 1440;
  bool get isDesktop => deviceType == AdaptiveScreenType.desktop;
  bool get isNativeTablet => deviceType == AdaptiveScreenType.nativeTablet;
  bool get isWebTablet => deviceType == AdaptiveScreenType.webTablet;
  bool get isWebMobile => deviceType == AdaptiveScreenType.webMobile;
  bool get isNativeMobile => deviceType == AdaptiveScreenType.nativeMobile;
}
