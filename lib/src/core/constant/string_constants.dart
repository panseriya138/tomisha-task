class StringConstant {
  StringConstant._();
  static const title0 = 'Drei einfache Schritte\nzu deinem neuen Job';
  static const title1 = 'Drei einfache Schritte\nzu deinem neuen Mitarbeiter';
  static const title2 =
      'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter';
  static const login = 'Login';
  static const jobWebsite = 'Deine Job\nwebsite';
  static const toggleButton1 = 'Arbeitnehmer';
  static const toggleButton2 = 'Arbeitgeber';
  static const toggleButton3 = 'Temporärbüro';
  static const firstPoint = 'Erstellen dein Unternehmensprofil';
  static const secondPoint = 'Erstellen ein Jobinserat';
  static const secondPointWeb = 'Erstellen dein Lebenslauf';
  static const thirdPoint = 'Wähle deinen neuen Mitarbeiter aus';
  static const arbitmerThirdPoint = 'Mit nur einem Klick\nbewerben';
  static const office1 = 'Erstellen dein Unternehmensprofil';
  static const office2 = 'Erhalte Vermittlungs- angebot von Arbeitgeber';
  static const office3 = 'Vermittlung nach Provision oder Stundenlohn';
  static const registrationButton = 'Kostenlos Registrieren';
  static const nameAppBar = 'Jetzt Klicken';
}
